package de.tudresden.inf.raf;

import grl.ContextProvider;
import grl.GRLspec;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import seg.jUCMNav.JUCMNavPlugin;

/**
 * Page to add a new ContextProvider
 * 
 */
public class AddContextProviderEditorPage extends WizardPage {
	private Text name;
	private Text nameSpace;

	private GRLspec grlSpec;

	private ModifyListener modifyListener = new ModifyListener() {
		public void modifyText(ModifyEvent e) {
			dialogChanged();
		}
	};
	private ContextProvider provider;

	/**
	 * The selection contains a contextCondition. Loaded in
	 * {@link #initialize()}.
	 * 
	 * @param selection
	 */
	public AddContextProviderEditorPage(GRLspec grlSpec, ContextProvider provider) {
		super("wizardPage"); //$NON-NLS-1$
		this.setImageDescriptor(ImageDescriptor.createFromFile(JUCMNavPlugin.class, "icons/perspectiveIcon.gif")); //$NON-NLS-1$
		this.grlSpec = grlSpec;
		this.provider = provider;
	}

	/**
	 * Creates the page.
	 */
	public void createControl(Composite parent) {
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "seg.jUCMNav.scenario_codeeditor"); //$NON-NLS-1$
		Composite container = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 1;
		layout.verticalSpacing = 5;

		// label over the code box.
		Label label = new Label(container, SWT.NULL);
		label.setText("Enter ContextProvider Name"); //$NON-NLS-1$

		// simple multi-line scrollable text-box that grows with box.
		name = new Text(container, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		if (provider != null) {
			name.setText(provider.getName());
		}
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		name.setLayoutData(gd);
		name.addModifyListener(modifyListener);

		// label over the provider box
		label = new Label(container, SWT.NULL);
		label.setText("Enter namespace of ContextProvider"); //$NON-NLS-1$

		nameSpace = new Text(container, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		nameSpace.setLayoutData(gd);
		nameSpace.addModifyListener(modifyListener);
		if (provider != null) {
			nameSpace.setText(provider.getNamespace());
		}

		setDescription("Enter values for name and namespace to create a new ContextProvider. Name is used as abbreviation in the queries, keep it short."); //$NON-NLS-1$
		setTitle("ContextProvider Creator"); //$NON-NLS-1$

		dialogChanged();
		setControl(container);
		name.forceFocus();
	}

	/**
	 * Ensures that the pseudo-code is legal (syntax and type checking)
	 */
	private void dialogChanged() {
		if (name.getText().isEmpty() || nameSpace.getText().isEmpty()) {
			setPageComplete(false);
		} else {
			setPageComplete(true);
		}
	}

	public String getProviderName() {
		return name.getText();
	}

	public String getNameSpace() {
		return nameSpace.getText();
	}
}
