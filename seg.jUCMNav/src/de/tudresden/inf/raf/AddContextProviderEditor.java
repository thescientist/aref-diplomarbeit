package de.tudresden.inf.raf;

import grl.ContextProvider;
import grl.GRLspec;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;

import seg.jUCMNav.model.ModelCreationFactory;
import seg.jUCMNav.model.util.URNNamingHelper;
import urn.URNspec;

/**
 * Wizard for editing a contextcondition.
 * 
 * @author sprasse
 */
public class AddContextProviderEditor extends Wizard {
	private AddContextProviderEditorPage page;
	private GRLspec grlSpec;
	private ContextProvider provider;

	/**
	 * The workbench page in which we are working
	 */
	protected IWorkbenchPage workbenchPage;

	/**
	 * Creates the editor
	 */
	public AddContextProviderEditor(ContextProvider provider) {
		super();
		setNeedsProgressMonitor(true);
		this.provider = provider;
		this.setWindowTitle("ContextCondition Editor"); //$NON-NLS-1$
	}

	/**
	 * Adding the page to the wizard.
	 */
	public void addPages() {
		page = new AddContextProviderEditorPage(grlSpec, provider);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using wizard as execution context.
	 */
	public boolean performFinish() {
		URNspec urn = grlSpec.getUrnspec();
		if (provider == null) {
			provider = (ContextProvider) ModelCreationFactory.getNewObject(urn, ContextProvider.class);
			URNNamingHelper.setElementNameAndID(urn, provider);
			grlSpec.getContextProviders().add(provider);
		}
		provider.setName(page.getProviderName());
		provider.setNamespace(page.getNameSpace());
		URNNamingHelper.resolveNamingConflict(urn, provider);
		return true;
	}

	/**
	 * Throws an error using the message.
	 * 
	 * @param message
	 *            the error message.
	 * @throws CoreException
	 *             the generated exception.
	 */
	private void throwCoreException(String message) throws CoreException {
		IStatus status = new Status(IStatus.ERROR, "seg.jUCMNav", IStatus.OK, message, null); //$NON-NLS-1$
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, GRLspec spec) {
		this.workbenchPage = workbench.getActiveWorkbenchWindow().getActivePage();
		this.grlSpec = spec;
	}
}