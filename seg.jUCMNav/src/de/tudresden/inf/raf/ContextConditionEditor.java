package de.tudresden.inf.raf;

import grl.ContextCondition;
import grl.ContextLabel;
import grl.ContextProvider;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.gef.commands.CommandStack;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;

import seg.jUCMNav.editors.UCMNavMultiPageEditor;

/**
 * Wizard for editing a contextcondition.
 * 
 * @author sprasse
 */
public class ContextConditionEditor extends Wizard {
	private ContextConditionEditorPage page;
	private ContextLabel defaultSelected;

	/**
	 * The workbench page in which we are working
	 */
	protected IWorkbenchPage workbenchPage;

	/**
	 * Creates the editor
	 */
	public ContextConditionEditor() {
		super();
		setNeedsProgressMonitor(true);
		this.setWindowTitle("ContextCondition Editor"); //$NON-NLS-1$
	}

	/**
	 * Adding the page to the wizard.
	 */
	public void addPages() {
		page = new ContextConditionEditorPage(defaultSelected);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using wizard as execution context.
	 */
	public boolean performFinish() {
		if (page.getSparqlExpression() != null && !page.getSparqlExpression().isEmpty()) {
			ContextCondition condition = defaultSelected.getLink().getLink().getCondition();
			List<ContextProvider> condProviders = new LinkedList<ContextProvider>();
			for (int selectionIdx : page.getcProvider().getSelectionIndices()) {
				condProviders.add(page.getProviderListMap().get(selectionIdx));
			}

			CommandStack cs = ((UCMNavMultiPageEditor) workbenchPage.getActiveEditor()).getDelegatingCommandStack();

			CompoundCommand cmd = new CompoundCommand();
			cmd.add(new ChangeContextConditionCommand(condition, page.getSparqlExpression(), page.getCondName(),
					condProviders));

			if (cmd.canExecute()) {
				cs.execute(cmd);
			}
		} else {
			defaultSelected.getLink().getLink().setCondition(null);
		}

		return true;
	}

	/**
	 * Throws an error using the message.
	 * 
	 * @param message
	 *            the error message.
	 * @throws CoreException
	 *             the generated exception.
	 */
	private void throwCoreException(String message) throws CoreException {
		IStatus status = new Status(IStatus.ERROR, "seg.jUCMNav", IStatus.OK, message, null); //$NON-NLS-1$
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(IWorkbench, IStructuredSelection)
	 */
	public void init(IWorkbench workbench, ContextLabel defaultSelected) {
		this.workbenchPage = workbench.getActiveWorkbenchWindow().getActivePage();
		this.defaultSelected = defaultSelected;
	}
}