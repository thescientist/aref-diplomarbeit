package de.tudresden.inf.raf;

import grl.ContextLabel;
import grl.ContextProvider;

import java.util.HashMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import seg.jUCMNav.JUCMNavPlugin;

/**
 * The page actually containing the code editor for responsibilities and
 * conditions.
 * 
 */
public class ContextConditionEditorPage extends WizardPage {
	private Text sparqlExpression;
	private Text condName;
	private List cProvider;
	private HashMap<Integer, ContextProvider> providerListMap;

	public List getcProvider() {
		return cProvider;
	}

	public ContextLabel getDefaultSelected() {
		return defaultSelected;
	}

	private ContextLabel defaultSelected;

	private ModifyListener modifyListener = new ModifyListener() {
		public void modifyText(ModifyEvent e) {
			dialogChanged();
		}
	};

	/**
	 * The selection contains a contextCondition. Loaded in
	 * {@link #initialize()}.
	 * 
	 * @param selection
	 */
	public ContextConditionEditorPage(ContextLabel defaultSelected) {
		super("wizardPage"); //$NON-NLS-1$

		this.setImageDescriptor(ImageDescriptor.createFromFile(JUCMNavPlugin.class, "icons/perspectiveIcon.gif")); //$NON-NLS-1$

		// loaded in initialize()
		this.defaultSelected = defaultSelected;
		providerListMap = new HashMap<Integer, ContextProvider>();
	}

	/**
	 * Creates the page.
	 */
	public void createControl(Composite parent) {
		PlatformUI.getWorkbench().getHelpSystem().setHelp(parent, "seg.jUCMNav.scenario_codeeditor"); //$NON-NLS-1$
		Composite container = new Composite(parent, SWT.NULL);

		GridLayout layout = new GridLayout();
		container.setLayout(layout);
		layout.numColumns = 2;
		layout.verticalSpacing = 5;

		GridData gridData = new GridData(GridData.VERTICAL_ALIGN_END);
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = GridData.FILL;
		Label label = new Label(container, SWT.NULL);
		label.setText("Name of condition (Used in Model as Labeltext):");
		label.setLayoutData(gridData);

		condName = new Text(container, SWT.BORDER | SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
		GridData gridData2 = new GridData(GridData.VERTICAL_ALIGN_END);
		gridData2.horizontalSpan = 2;
		gridData2.horizontalAlignment = GridData.BEGINNING;
		condName.setLayoutData(gridData2);
		// label over the code box.
		label = new Label(container, SWT.NULL);
		label.setText("Enter SPARQL Expression"); //$NON-NLS-1$

		// label over the provider box
		label = new Label(container, SWT.NULL);
		label.setText("Select Context Provider"); //$NON-NLS-1$

		// simple multi-line scrollable text-box that grows with box.
		sparqlExpression = new Text(container, SWT.BORDER | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);

		GridData gd = new GridData(GridData.FILL_BOTH);
		sparqlExpression.setLayoutData(gd);
		sparqlExpression.addModifyListener(modifyListener);

		// variable list
		cProvider = new List(container, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);

		gd = new GridData(GridData.FILL_VERTICAL);

		// gd.widthHint = 100;
		// gd.heightHint = 200;
		cProvider.setLayoutData(gd);

		// Button to open the new context Provider wizard.
		Button button = new Button(container, SWT.PUSH);
		button.setText("Create Context Provider"); //$NON-NLS-1$
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				AddContextProviderEditor wizard = new AddContextProviderEditor(null);

				wizard.init(PlatformUI.getWorkbench(), defaultSelected.getLink().getLink().getGrlspec());
				WizardDialog dialog = new WizardDialog(shell, wizard);
				dialog.setBlockOnOpen(true);
				if (dialog.open() == Window.OK) {
					initProviders();
				}
			}
		});
		Button button2 = new Button(container, SWT.PUSH);
		button2.setText("Edit Context Provider"); //$NON-NLS-1$
		button2.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
				if (getcProvider().getSelectionIndices() != null && getcProvider().getSelectionIndices().length != 0) {
					ContextProvider provider = providerListMap.get(getcProvider().getSelectionIndices()[0]);
					AddContextProviderEditor wizard = new AddContextProviderEditor(provider);

					wizard.init(PlatformUI.getWorkbench(), defaultSelected.getLink().getLink().getGrlspec());
					WizardDialog dialog = new WizardDialog(shell, wizard);
					dialog.setBlockOnOpen(true);
					if (dialog.open() == Window.OK) {
						initProviders();
					}
				}
			}
		});
		// TODO Remove ContextProvider
		initialize();

		setDescription("Select ContextProvider and enter a SPARQL Expression"); //$NON-NLS-1$
		setTitle("ContextCondition Editor"); //$NON-NLS-1$

		dialogChanged();
		setControl(container);
		sparqlExpression.forceFocus();

	}

	public String getCondName() {
		return condName.getText();
	}

	/**
	 * Tests if the current workbench selection is a suitable container to use.
	 */
	private void initialize() {
		initProviders();
		setupText();
	}

	private void setupText() {
		String oldSparql = defaultSelected.getLink().getLink().getCondition().getSparqlExpression();
		if (oldSparql != null) {
			sparqlExpression.setText(oldSparql);
		}
		String oldName = defaultSelected.getLink().getLink().getCondition().getName();
		if (oldName != null) {
			condName.setText(oldName);
		}
	}

	public HashMap<Integer, ContextProvider> getProviderListMap() {
		return providerListMap;
	}

	private void initProviders() {
		cProvider.removeAll();
		EList specProviders = defaultSelected.getLink().getDiagram().getUrndefinition().getUrnspec().getGrlspec()
				.getContextProviders();
		EList oldProvider = defaultSelected.getLink().getLink().getCondition().getProvider();
		for (Object object : specProviders) {
			if (object instanceof ContextProvider) {
				ContextProvider provider = (ContextProvider) object;
				cProvider.add(provider.getName() + " - " + provider.getNamespace(), cProvider.getItemCount());
				providerListMap.put(cProvider.getItemCount() - 1, provider);
				if (oldProvider.contains(provider)) {
					cProvider.select(cProvider.getItemCount() - 1);
				}
			}
		}
		// for (Object object : oldProvider) {
		// if (object instanceof ContextProvider) {
		// ContextProvider provider = (ContextProvider) object;
		// cProvider.select(providerListMap.get(provider));
		// }
		// }
	}

	/**
	 * Ensures that the pseudo-code is legal (syntax and type checking)
	 */
	private void dialogChanged() {
		if (sparqlExpression.getText().isEmpty() || !sparqlExpression.getText().isEmpty()
				&& cProvider.getSelectionCount() == 0 || condName.getText().isEmpty()) {
			setPageComplete(false);
		} else {
			setPageComplete(true);
		}
	}

	/**
	 * The expression entered in the text box.
	 * 
	 * @return the expression.
	 */
	public String getSparqlExpression() {
		return sparqlExpression.getText();
	}

}
