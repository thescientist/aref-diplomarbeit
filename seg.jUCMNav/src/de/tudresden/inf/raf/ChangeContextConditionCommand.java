/*
 * Copyright © 2013 Stefan Prasse. All Rights Reserved. See LICENSE file of
 * project for more information.
 */
package de.tudresden.inf.raf;

import grl.ContextCondition;
import grl.ContextProvider;

import java.util.List;

import org.eclipse.gef.commands.Command;

import seg.jUCMNav.model.commands.JUCMNavCommand;

/**
 * @author Stefan Prasse
 * 
 */
public class ChangeContextConditionCommand extends Command implements JUCMNavCommand {

	ContextCondition condition;
	String query, oldQuery;
	String name, oldName;
	List<ContextProvider> provider, oldProvider;

	public ChangeContextConditionCommand(ContextCondition condition, String newQuery, String newName,
			List<ContextProvider> newProvider) {

		this.condition = condition;
		this.name = newName;
		this.query = newQuery;
		this.provider = newProvider;

		setLabel();
	}

	private void setLabel() {
		setLabel("ChangeContextConditionCommand"); //$NON-NLS-1$
	}

	/**
	 * @see org.eclipse.gef.commands.Command#execute()
	 */
	public void execute() {
		oldQuery = condition.getSparqlExpression();
		oldName = condition.getName();
		oldProvider = condition.getProvider();
		redo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.gef.commands.Command#redo()
	 */
	public void redo() {
		testPreConditions();

		condition.setName(name);
		condition.setSparqlExpression(query);
		condition.getProvider().clear();
		for (ContextProvider cp : provider) {
			condition.getProvider().add(cp);
		}

		testPostConditions();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see seg.jUCMNav.model.commands.JUCMNavCommand#testPostConditions()
	 */
	public void testPostConditions() {
		// TODO unique name
		assert condition != null : "post no elemement to name!"; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see seg.jUCMNav.model.commands.JUCMNavCommand#testPreConditions()
	 */
	public void testPreConditions() {
		assert condition != null : "pre no elemement to name!"; //$NON-NLS-1$

	}

	/**
	 * @see org.eclipse.gef.commands.Command#undo()
	 */
	public void undo() {
		testPostConditions();

		condition.setName(oldName);
		condition.setSparqlExpression(oldQuery);
		condition.getProvider().clear();
		for (ContextProvider cp : oldProvider) {
			condition.getProvider().add(cp);
		}

		testPreConditions();
	}
}
