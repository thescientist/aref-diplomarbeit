/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package grl;

import urncore.GRLmodelElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Context Provider</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link grl.ContextProvider#getNamespace <em>Namespace</em>}</li>
 *   <li>{@link grl.ContextProvider#getGrlspec <em>Grlspec</em>}</li>
 * </ul>
 * </p>
 *
 * @see grl.GrlPackage#getContextProvider()
 * @model
 * @generated
 */
public interface ContextProvider extends GRLmodelElement {
	/**
	 * Returns the value of the '<em><b>Namespace</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Namespace</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Namespace</em>' attribute.
	 * @see #setNamespace(String)
	 * @see grl.GrlPackage#getContextProvider_Namespace()
	 * @model required="true"
	 * @generated
	 */
	String getNamespace();

	/**
	 * Sets the value of the '{@link grl.ContextProvider#getNamespace
	 * <em>Namespace</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Namespace</em>' attribute.
	 * @see #getNamespace()
	 * @generated
	 */
	void setNamespace(String value);

	/**
	 * Returns the value of the '<em><b>Grlspec</b></em>' container reference.
	 * It is bidirectional and its opposite is '
	 * {@link grl.GRLspec#getContextProviders <em>Context Providers</em>}'. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grlspec</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Grlspec</em>' container reference.
	 * @see #setGrlspec(GRLspec)
	 * @see grl.GrlPackage#getContextProvider_Grlspec()
	 * @see grl.GRLspec#getContextProviders
	 * @model opposite="contextProviders" required="true" transient="false"
	 * @generated
	 */
	GRLspec getGrlspec();

	/**
	 * Sets the value of the '{@link grl.ContextProvider#getGrlspec <em>Grlspec</em>}' container reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Grlspec</em>' container reference.
	 * @see #getGrlspec()
	 * @generated
	 */
	void setGrlspec(GRLspec value);

} // ContextProvider
