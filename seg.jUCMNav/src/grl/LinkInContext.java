/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package grl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link In Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link grl.LinkInContext#getContexts <em>Contexts</em>}</li>
 *   <li>{@link grl.LinkInContext#getLink <em>Link</em>}</li>
 *   <li>{@link grl.LinkInContext#getSparqlExpression <em>Sparql Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see grl.GrlPackage#getLinkInContext()
 * @model
 * @generated
 */
public interface LinkInContext extends EObject {
	/**
	 * Returns the value of the '<em><b>Contexts</b></em>' reference list.
	 * The list contents are of type {@link grl.ContextProvider}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contexts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contexts</em>' reference list.
	 * @see grl.GrlPackage#getLinkInContext_Contexts()
	 * @model type="grl.ContextProvider" required="true"
	 * @generated
	 */
	EList getContexts();

	/**
	 * Returns the value of the '<em><b>Link</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link grl.ElementLink#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link</em>' container reference.
	 * @see #setLink(ElementLink)
	 * @see grl.GrlPackage#getLinkInContext_Link()
	 * @see grl.ElementLink#getContext
	 * @model opposite="context" required="true" transient="false"
	 * @generated
	 */
	ElementLink getLink();

	/**
	 * Sets the value of the '{@link grl.LinkInContext#getLink <em>Link</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link</em>' container reference.
	 * @see #getLink()
	 * @generated
	 */
	void setLink(ElementLink value);

	/**
	 * Returns the value of the '<em><b>Sparql Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sparql Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sparql Expression</em>' attribute.
	 * @see #setSparqlExpression(String)
	 * @see grl.GrlPackage#getLinkInContext_SparqlExpression()
	 * @model required="true"
	 * @generated
	 */
	String getSparqlExpression();

	/**
	 * Sets the value of the '{@link grl.LinkInContext#getSparqlExpression <em>Sparql Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sparql Expression</em>' attribute.
	 * @see #getSparqlExpression()
	 * @generated
	 */
	void setSparqlExpression(String value);

} // LinkInContext
