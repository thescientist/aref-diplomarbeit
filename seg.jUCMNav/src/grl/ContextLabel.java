/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package grl;

import urncore.Label;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Context Label</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link grl.ContextLabel#getLink <em>Link</em>}</li>
 * </ul>
 * </p>
 *
 * @see grl.GrlPackage#getContextLabel()
 * @model
 * @generated
 */
public interface ContextLabel extends Label {
	/**
	 * Returns the value of the '<em><b>Link</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link grl.LinkRef#getContextLabel <em>Context Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link</em>' container reference.
	 * @see #setLink(LinkRef)
	 * @see grl.GrlPackage#getContextLabel_Link()
	 * @see grl.LinkRef#getContextLabel
	 * @model opposite="contextLabel" required="true" transient="false"
	 * @generated
	 */
	LinkRef getLink();

	/**
	 * Sets the value of the '{@link grl.ContextLabel#getLink <em>Link</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link</em>' container reference.
	 * @see #getLink()
	 * @generated
	 */
	void setLink(LinkRef value);

} // ContextLabel
