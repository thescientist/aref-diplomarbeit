/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package grl;

import org.eclipse.emf.common.util.EList;

import urncore.GRLmodelElement;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>Context Condition</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link grl.ContextCondition#getProvider <em>Provider</em>}</li>
 *   <li>{@link grl.ContextCondition#getSparqlExpression <em>Sparql Expression</em>}</li>
 *   <li>{@link grl.ContextCondition#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see grl.GrlPackage#getContextCondition()
 * @model
 * @generated
 */
public interface ContextCondition extends GRLmodelElement {
	/**
	 * Returns the value of the '<em><b>Provider</b></em>' reference list. The
	 * list contents are of type {@link grl.ContextProvider}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provider</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Provider</em>' reference list.
	 * @see grl.GrlPackage#getContextCondition_Provider()
	 * @model type="grl.ContextProvider" required="true"
	 * @generated
	 */
	EList getProvider();

	/**
	 * Returns the value of the '<em><b>Sparql Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sparql Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sparql Expression</em>' attribute.
	 * @see #setSparqlExpression(String)
	 * @see grl.GrlPackage#getContextCondition_SparqlExpression()
	 * @model required="true"
	 * @generated
	 */
	String getSparqlExpression();

	/**
	 * Sets the value of the '{@link grl.ContextCondition#getSparqlExpression <em>Sparql Expression</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Sparql Expression</em>' attribute.
	 * @see #getSparqlExpression()
	 * @generated
	 */
	void setSparqlExpression(String value);

	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Element</em>' reference.
	 * @see #setElement(GRLmodelElement)
	 * @see grl.GrlPackage#getContextCondition_Element()
	 * @model required="true"
	 * @generated
	 */
	GRLmodelElement getElement();

	/**
	 * Sets the value of the '{@link grl.ContextCondition#getElement
	 * <em>Element</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Element</em>' reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(GRLmodelElement value);

} // ContextCondition
