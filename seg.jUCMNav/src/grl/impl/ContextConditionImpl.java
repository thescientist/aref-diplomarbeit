/**
 * <copyright> </copyright>
 * 
 * $Id$
 */
package grl.impl;

import grl.ContextCondition;
import grl.ContextProvider;
import grl.GrlPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import urncore.GRLmodelElement;
import urncore.impl.GRLmodelElementImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Context Condition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link grl.impl.ContextConditionImpl#getProvider <em>Provider</em>}</li>
 *   <li>{@link grl.impl.ContextConditionImpl#getSparqlExpression <em>Sparql Expression</em>}</li>
 *   <li>{@link grl.impl.ContextConditionImpl#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ContextConditionImpl extends GRLmodelElementImpl implements ContextCondition {
	/**
	 * The cached value of the '{@link #getProvider() <em>Provider</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getProvider()
	 * @generated
	 * @ordered
	 */
	protected EList provider;

	/**
	 * The default value of the '{@link #getSparqlExpression() <em>Sparql Expression</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSparqlExpression()
	 * @generated
	 * @ordered
	 */
	protected static final String SPARQL_EXPRESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSparqlExpression() <em>Sparql Expression</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSparqlExpression()
	 * @generated
	 * @ordered
	 */
	protected String sparqlExpression = SPARQL_EXPRESSION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getElement() <em>Element</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getElement()
	 * @generated
	 * @ordered
	 */
	protected GRLmodelElement element;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ContextConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return GrlPackage.Literals.CONTEXT_CONDITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList getProvider() {
		if (provider == null) {
			provider = new EObjectResolvingEList(ContextProvider.class, this, GrlPackage.CONTEXT_CONDITION__PROVIDER);
		}
		return provider;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getSparqlExpression() {
		return sparqlExpression;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSparqlExpression(String newSparqlExpression) {
		String oldSparqlExpression = sparqlExpression;
		sparqlExpression = newSparqlExpression;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GrlPackage.CONTEXT_CONDITION__SPARQL_EXPRESSION, oldSparqlExpression, sparqlExpression));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GRLmodelElement getElement() {
		if (element != null && element.eIsProxy()) {
			InternalEObject oldElement = (InternalEObject)element;
			element = (GRLmodelElement)eResolveProxy(oldElement);
			if (element != oldElement) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GrlPackage.CONTEXT_CONDITION__ELEMENT, oldElement, element));
			}
		}
		return element;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public GRLmodelElement basicGetElement() {
		return element;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement(GRLmodelElement newElement) {
		GRLmodelElement oldElement = element;
		element = newElement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GrlPackage.CONTEXT_CONDITION__ELEMENT, oldElement, element));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GrlPackage.CONTEXT_CONDITION__PROVIDER:
				return getProvider();
			case GrlPackage.CONTEXT_CONDITION__SPARQL_EXPRESSION:
				return getSparqlExpression();
			case GrlPackage.CONTEXT_CONDITION__ELEMENT:
				if (resolve) return getElement();
				return basicGetElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GrlPackage.CONTEXT_CONDITION__PROVIDER:
				getProvider().clear();
				getProvider().addAll((Collection)newValue);
				return;
			case GrlPackage.CONTEXT_CONDITION__SPARQL_EXPRESSION:
				setSparqlExpression((String)newValue);
				return;
			case GrlPackage.CONTEXT_CONDITION__ELEMENT:
				setElement((GRLmodelElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID) {
			case GrlPackage.CONTEXT_CONDITION__PROVIDER:
				getProvider().clear();
				return;
			case GrlPackage.CONTEXT_CONDITION__SPARQL_EXPRESSION:
				setSparqlExpression(SPARQL_EXPRESSION_EDEFAULT);
				return;
			case GrlPackage.CONTEXT_CONDITION__ELEMENT:
				setElement((GRLmodelElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GrlPackage.CONTEXT_CONDITION__PROVIDER:
				return provider != null && !provider.isEmpty();
			case GrlPackage.CONTEXT_CONDITION__SPARQL_EXPRESSION:
				return SPARQL_EXPRESSION_EDEFAULT == null ? sparqlExpression != null : !SPARQL_EXPRESSION_EDEFAULT.equals(sparqlExpression);
			case GrlPackage.CONTEXT_CONDITION__ELEMENT:
				return element != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sparqlExpression: ");
		result.append(sparqlExpression);
		result.append(')');
		return result.toString();
	}

} // ContextConditionImpl
