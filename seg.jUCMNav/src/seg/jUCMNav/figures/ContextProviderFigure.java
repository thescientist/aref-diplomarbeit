package seg.jUCMNav.figures;

import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

/**
 * Figure for ContextProviderEditParts. *
 * 
 * @author Stefan Prasse
 */
public class ContextProviderFigure extends GrlNodeFigure {

	public ContextProviderFigure() {
		super();
		autoResize = false;
	}

	@Override
	protected void outlineShape(Graphics graphics) {
		Rectangle r = getBounds().getCopy();
		r.x += getLineWidth() / 2;
		r.y += getLineWidth() / 2;
		r.width -= getLineWidth();
		r.height -= getLineWidth();
		setColors(null, "240,240,240", true);
		graphics.drawRoundRectangle(r, 15, 15);
	}

	@Override
	protected void fillShape(Graphics graphics) {
		Rectangle r = getBounds().getCopy();
		r.x += getLineWidth() / 2;
		r.y += getLineWidth() / 2;
		r.width -= getLineWidth();
		r.height -= getLineWidth();
		setColors(null, "240,240,240", true);
		graphics.drawRoundRectangle(r, 15, 15);
	}

}
