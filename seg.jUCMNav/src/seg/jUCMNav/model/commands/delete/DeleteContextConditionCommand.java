/**
 * 
 */
package seg.jUCMNav.model.commands.delete;

import grl.LinkRef;

import org.eclipse.gef.commands.CompoundCommand;

import seg.jUCMNav.Messages;

/**
 * Delete an actor definition.
 * 
 * @author sprasse
 * 
 */
public class DeleteContextConditionCommand extends CompoundCommand {

	private LinkRef linkRef;

	/**
	 * @param condition
	 *            the condition to delete
	 */
	public DeleteContextConditionCommand(LinkRef linkRef) {
		setLabel(Messages.getString("DeleteActorCommand.deleteActor")); //$NON-NLS-1$
		this.linkRef = linkRef;
	}

	/**
	 * Returns true even if no commands exist.
	 */
	public boolean canExecute() {
		if (getCommands().size() == 0)
			return true;
		else
			return super.canExecute();
	}

	/**
	 * Returns true even if no commands exist.
	 */
	public boolean canUndo() {
		if (getCommands().size() == 0)
			return true;
		else
			return super.canUndo();
	}

	/**
	 * Late building
	 */
	public void execute() {
		build();
		super.execute();
	}

	/**
	 * Build the compound command.
	 * 
	 */
	private void build() {
		linkRef.getLink().setCondition(null);
	}

}
