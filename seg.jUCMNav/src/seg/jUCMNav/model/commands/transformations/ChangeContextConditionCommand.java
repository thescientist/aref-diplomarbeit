package seg.jUCMNav.model.commands.transformations;

import grl.ContextCondition;
import grl.LinkRef;

import org.eclipse.gef.commands.Command;

import seg.jUCMNav.model.ModelCreationFactory;
import seg.jUCMNav.model.commands.JUCMNavCommand;

/**
 * Command to change the qualitative value of a GRL contribution
 * 
 * @author Stefan Prasse
 */

public class ChangeContextConditionCommand extends Command implements JUCMNavCommand {

	private LinkRef linkRef;
	private ContextCondition oldCondition;

	public ChangeContextConditionCommand(LinkRef linkRef) {
		setLabel("Change ContextContribution Command"); //$NON-NLS-1$
		this.linkRef = linkRef;
	}

	public void execute() {
		redo();
	}

	public void redo() {
		testPreConditions();

		ContextCondition condition = linkRef.getLink().getCondition();
		if (condition == null) {
			condition = (ContextCondition) ModelCreationFactory.getNewObject(linkRef.getDiagram().getUrndefinition()
					.getUrnspec(), ContextCondition.class);
		} else {
			oldCondition = condition;
		}
		linkRef.getLink().setCondition(condition);
		condition.setElement(linkRef.getLink());
		testPostConditions();
	}

	public void testPostConditions() {
		assert linkRef.getLink().getCondition() != null : "post no element!"; //$NON-NLS-1$
	}

	public void testPreConditions() {
		assert true;
	}

	public void undo() {
		testPostConditions();

		linkRef.getLink().setCondition(oldCondition);

		testPreConditions();
	}
}
