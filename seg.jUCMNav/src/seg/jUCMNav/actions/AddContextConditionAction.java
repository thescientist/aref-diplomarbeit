package seg.jUCMNav.actions;

import grl.LinkRef;

import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IWorkbenchPart;

import seg.jUCMNav.editparts.LinkRefEditPart;
import seg.jUCMNav.model.commands.transformations.ChangeContextConditionCommand;

/**
 * 
 * @author damyot
 */

public class AddContextConditionAction extends URNSelectionAction {
	public static final String ADD_CONTEXT_CONDITION = "seg.jUCMNav.ADD_CONTEXT_CONDITION";
	private LinkRef linkRef;

	public AddContextConditionAction(IWorkbenchPart part) {
		super(part);
		setId(ADD_CONTEXT_CONDITION);
		setText("TestText");
	}

	protected Command getCommand() {
		return new ChangeContextConditionCommand(linkRef);
	}

	/**
	 * We need to have a link reference selected. For increase and decrease
	 * operations verify if possible
	 */
	public boolean isEnabled() {
		List selection = getSelectedObjects();
		if (!selection.isEmpty()) {
			Object o = selection.get(0);
			if (o instanceof LinkRef) {
				linkRef = (LinkRef) o;
				return true;
			} else if (o instanceof LinkRefEditPart) {
				linkRef = ((LinkRefEditPart) o).getLinkRef();
				return true;
			}
		}
		return false;
	}

	public String getId(String operation) {
		return ADD_CONTEXT_CONDITION;
	}
}
