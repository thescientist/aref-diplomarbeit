package seg.jUCMNav.actions;

import grl.LinkRef;

import java.util.List;

import org.eclipse.gef.commands.Command;
import org.eclipse.ui.IWorkbenchPart;

import seg.jUCMNav.editparts.LinkRefEditPart;
import seg.jUCMNav.model.commands.delete.DeleteContextConditionCommand;

/**
 * 
 * @author damyot
 */

public class DeleteContextConditionAction extends URNSelectionAction {
	public static final String DEL_CONTEXT_CONDITION = "seg.jUCMNav.DEL_CONTEXT_CONDITION";
	private LinkRef linkRef;

	public DeleteContextConditionAction(IWorkbenchPart part) {
		super(part);
		setId(DEL_CONTEXT_CONDITION);
		setText("Delete ContextCondition");
	}

	protected Command getCommand() {
		return new DeleteContextConditionCommand(linkRef);
	}

	/**
	 * We need to have a link reference selected. For increase and decrease
	 * operations verify if possible
	 */
	public boolean isEnabled() {
		List selection = getSelectedObjects();
		if (!selection.isEmpty()) {
			Object o = selection.get(0);
			if (o instanceof LinkRef) {
				linkRef = (LinkRef) o;
				if (linkRef.getLink().getCondition() != null) {
					return true;
				}
			} else if (o instanceof LinkRefEditPart) {
				linkRef = ((LinkRefEditPart) o).getLinkRef();
				if (linkRef.getLink().getCondition() != null) {
					return true;
				}
			}
		}
		return false;
	}

	public String getId(String operation) {
		return DEL_CONTEXT_CONDITION;
	}
}
