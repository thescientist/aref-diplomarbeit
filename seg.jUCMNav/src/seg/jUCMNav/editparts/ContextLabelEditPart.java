package seg.jUCMNav.editparts;

import grl.ContextCondition;
import grl.ContextLabel;
import grl.ElementLink;
import grl.LinkRef;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.RoutingListener;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import seg.jUCMNav.figures.ColorManager;
import seg.jUCMNav.figures.LabelFigure;
import seg.jUCMNav.figures.LinkRefConnection;
import urncore.IURNConnection;
import urncore.Label;
import de.tudresden.inf.raf.ContextConditionEditor;

/**
 * Editpart associated with urncore.ConnectionLabel; a special label.
 * 
 * @author etremblay
 */
public class ContextLabelEditPart extends LabelEditPart {
	private Image img;
	private RoutingListener routingListener;

	public ContextLabelEditPart(ContextLabel model) {
		super(model);
		if (model.getLink() != null)
			modelElement = model.getLink();
	}

	/**
	 * Opens the direct edit manager.
	 * 
	 */
	protected void performDirectEdit() {
		// open condition editor
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		ContextConditionEditor wizard = new ContextConditionEditor();

		wizard.init(PlatformUI.getWorkbench(), (ContextLabel) getModel());
		WizardDialog dialog = new WizardDialog(shell, wizard);
		dialog.open();

	}

	@Override
	public void activate() {
		LinkRefEditPart nc = (LinkRefEditPart) getViewer().getEditPartRegistry().get(getConnection());

		routingListener = new RoutingListener.Stub() {
			public void postRoute(Connection connection) {
				refreshVisuals();
			}
		};

		if (nc != null)
			((LinkRefConnection) nc.getConnectionFigure()).addRoutingListener(routingListener);
		super.activate();
	}

	@Override
	public void deactivate() {
		LinkRefEditPart nc = (LinkRefEditPart) getViewer().getEditPartRegistry().get(getConnection());

		if (nc != null)
			((LinkRefConnection) nc.getConnectionFigure()).removeRoutingListener(routingListener);
		super.deactivate();
	}

	/**
	 * Places labels on the screen given their size, the model element's
	 * position and the deltax/y.
	 * 
	 * NodeConnection conditions are placed relative to the middle of the node
	 * connection.
	 * 
	 * For pre/post-conditions, refer to superclass.
	 * 
	 */
	protected Point calculateModelElementPosition(Label label, Dimension labelDimension) {
		Point location = new Point(0, 0);

		if (getURNmodelElement() instanceof IURNConnection && label != null && labelDimension != null) {
			LinkRefEditPart nc = (LinkRefEditPart) getViewer().getEditPartRegistry().get(getConnection());
			if (nc != null) {
				/**
				 * The position of a ConnectionLabel is calculated
				 * perpendicularly to the connection. In fact perpendicular to
				 * the last segment of the connection if the connection has
				 * bendpoints. If we consider the last segment of the connection
				 * as the vector A, then deltaY is added in the direction of
				 * vector A. DeltaX is added in the direction of the normal of
				 * A.
				 * 
				 * We simply use vector addition to get the final position x, y
				 * of the label.
				 * 
				 * See AbstractDiagramXYLayoutEditPolicy to see how we get the
				 * deltaX, deltaY when the user moves the label
				 */

				PointList points = nc.getConnectionFigure().getPoints();
				Point last = points.getLastPoint();
				Point preLast = points.getPoint(points.size() - 2);

				Dimension normal = new Dimension(last.x - preLast.x, last.y - preLast.y);
				double normalL = length(normal.width, normal.height);

				double alpha = Math.asin(normal.height / normalL);
				int quadrant = calculateQuadrant(normal);

				if (quadrant == 2 || quadrant == 3)
					alpha = Math.PI - alpha;

				double b1 = label.getDeltaY() * Math.cos(alpha) + label.getDeltaX() * Math.sin(alpha)
						- labelDimension.width / 2;
				double b2 = label.getDeltaY() * Math.sin(alpha) - label.getDeltaX() * Math.cos(alpha)
						- labelDimension.height / 2;

				location = new Point((int) b1 + last.x, (int) b2 + last.y);
			}
			return location;
		}

		return super.calculateModelElementPosition(label, labelDimension);
	}

	private double dotProduct(double p1x, double p1y, double p2x, double p2y) {
		return p1x * p2x + p1y * p2y;
	}

	private double length(double px, double py) {
		return Math.sqrt(Math.pow(px, 2) + Math.pow(py, 2));
	}

	private int calculateQuadrant(Dimension vect) {
		if (vect.height > 0) {
			if (vect.width > 0)
				return 1;
			else
				return 2;
		} else {
			if (vect.width > 0)
				return 4;
			else
				return 3;
		}
	}

	/**
	 * Returns the node connection; no check to see if we are actually linking
	 * to a node connection
	 * 
	 * @return the node connection on which the ConnectionLabel is located
	 */
	public IURNConnection getConnection() {
		return (IURNConnection) getURNmodelElement();
	}

	/**
	 * Reverts to existing name in model when exiting from a direct edit
	 * (possibly before a commit which will result in a change in the label
	 * value)
	 */
	public void revertNameChange() {
		LabelFigure tableFigure = (LabelFigure) getFigure();
		tableFigure.setVisible(true);
		// remove surrounding []
		if (tableFigure.getEditableText().startsWith("[") && tableFigure.getEditableText().length() > 2) //$NON-NLS-1$
			tableFigure.setEditableText(tableFigure.getEditableText().substring(1,
					tableFigure.getEditableText().length() - 1));

		refreshVisuals();
	}

	/**
	 * Sets the text in the label and its properties. make it surrounded by []
	 * and of a lighter color.
	 */
	protected void setLabelText() {
		LabelFigure labelFigure = getLabelFigure();
		if (getModel() == ((LinkRef) getConnection()).getContextLabel()) {
			if (((LinkRef) getConnection()).getLink() instanceof ElementLink) {
				ContextCondition condition = ((LinkRef) getConnection()).getLink().getCondition();
				if (condition != null) {
					labelFigure.setText(condition.getName());
					labelFigure.setBackgroundColor(ColorManager.DARKGRAY);
					labelFigure.setVisible(true);
				}
			}
		}
	}
}