package seg.jUCMNav.editparts.treeEditparts;

import grl.ContextProvider;

import org.eclipse.gef.EditPolicy;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.TreeItem;

import seg.jUCMNav.JUCMNavPlugin;
import seg.jUCMNav.editpolicies.directEditPolicy.GrlNodeDirectEditPolicy;
import seg.jUCMNav.figures.ColorManager;

/**
 * TreeEditPart for the intentional elements
 * 
 * @author Jean-Francois Roy, pchen
 * 
 */
public class ContextProviderTreeEditPart extends UrnModelElementTreeEditPart {

	/**
	 * @param model
	 *            the intentionalElement definition
	 */
	public ContextProviderTreeEditPart(Object model) {
		super(model);
	}

	/**
	 * @see org.eclipse.gef.editparts.AbstractEditPart#createEditPolicies()
	 */
	protected void createEditPolicies() {
		installEditPolicy(EditPolicy.COMPONENT_ROLE, new GrlNodeDirectEditPolicy());
	}

	/**
	 * @return the intentional element definition
	 */
	protected ContextProvider getElement() {
		return (ContextProvider) getModel();
	}

	/**
	 * Returns the icon appropriate for this element type
	 */
	protected Image getImage() {
		if (super.getImage() == null) {
			setImage((JUCMNavPlugin.getImage("icons/Resource16.gif"))); //$NON-NLS-1$
		}
		return super.getImage();
	}

	/**
	 * Sets unused definitions to a lighter color.
	 * 
	 * @see org.eclipse.gef.editparts.AbstractTreeEditPart#refreshVisuals()
	 */
	protected void refreshVisuals() {
		if (widget == null)
			return;
		((TreeItem) widget).setForeground(ColorManager.BLACK);
		getImage();
		super.refreshVisuals();
	}
}
