/*
 * Copyright � 2013 Stefan Prasse. All Rights Reserved. See LICENSE file of
 * project for more information.
 */
package de.tud.inf.aref;

import grl.Actor;
import grl.ContextCondition;
import grl.ElementLink;
import grl.GRLspec;
import grl.IntentionalElement;
import urncore.GRLmodelElement;

/**
 * Functions useful for Requirements Reflection. Adaptive Systems should have
 * the ability to modify the Requirements model.
 * 
 * @author Stefan Prasse
 */
public abstract class AbstractRequirementsReflection {
	/**
	 * Create new Actor in GRL Specification.
	 */
	public abstract void createActor(GRLspec specification, Actor newActor);

	/**
	 * Move an IntentionalElement to another Actor. The recursive flag indicates
	 * if children of this element should be moved to the new Actor too. This
	 * can only be done, if the given element is the only parent. Otherwise
	 * dependencies to the old Actor will be created.
	 */
	public abstract void moveIntentionalElementToActor(IntentionalElement element, Actor actor, boolean recursive);

	/**
	 * Add a new IntentionalElement to an actor.
	 */
	public abstract void addIntentionalElementToActor(IntentionalElement newElement, Actor actor);

	/**
	 * Modify or create (if not existing) an ElementLink of the GRL
	 * Specification. Possible Modifications are changing the type, source or
	 * target of the ElementLink.
	 */
	public abstract void modifyElementLink(GRLspec specification, ElementLink link);

	/**
	 * Modify or create (if not existing) a ContextCondition of a GRL model
	 * element (intentional element or element link). Reference ContextProvider
	 * should also be added to the GRL Specification, if the don't already
	 * exist.
	 */
	public abstract void modifyContextCondition(GRLmodelElement target, ContextCondition condition);
}
