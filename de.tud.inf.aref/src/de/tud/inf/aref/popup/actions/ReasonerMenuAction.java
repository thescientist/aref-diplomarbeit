package de.tud.inf.aref.popup.actions;

import grl.ContextProvider;
import grl.GRLspec;
import grl.IntentionalElement;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import urn.URNspec;
import urn.UrnPackage;
import de.tud.inf.aref.util.GrlUtil;
import de.tud.inf.aref.util.OntologyUtil;

public class ReasonerMenuAction implements IObjectActionDelegate {

	private Shell shell;

	private IFile selectedFile;

	private static MessageConsole instance = null;

	/**
	 * Constructor for Action1.
	 */
	public ReasonerMenuAction() {
		super();
	}

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
		Object o = targetPart.getSite().getSelectionProvider().getSelection();
		if (o instanceof TreeSelection) {
			Object file = ((TreeSelection) o).getFirstElement();
			if (file instanceof IFile) {
				selectedFile = (IFile) file;
				return;
			}
		}
		selectedFile = null;
	}

	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		URNspec model = null;

		// Load Model from file
		UrnPackage.eINSTANCE.eClass();

		URI uri = URI.createFileURI(selectedFile.getFullPath().toString());
		Resource resource = new XMIResourceImpl();
		resource.unload();
		resource.setURI(uri);
		try {
			resource.load(null);
			for (Object o : resource.getContents()) {
				if (o instanceof URNspec) {
					model = (URNspec) o;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (model == null) {
			return;
		}
		GRLspec grl = model.getGrlspec();
		for (Object object : grl.getContextProviders()) {
			if (object instanceof ContextProvider) {
				ContextProvider provider = (ContextProvider) object;
				InputDialog input = new InputDialog(shell, "Enter ontologies path or uri!",
						"Enter ontologies path or uri for ontology with namespace " + provider.getNamespace() + "!",
						"", null);
				input.open();
				String path = input.getValue();

				OntologyUtil.addContextProvider(provider, path);
			}
		}

		List<IntentionalElement> roots = GrlUtil.getRootElements(grl);
		for (IntentionalElement ie : roots) {
			ReasonerMenuAction.consoleOut("Reasoning from root element: " + ie.getName());
			GrlUtil.reason(model, ie, true);
		}
		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		// Create a resource
		Resource resourceNew = resSet.createResource(URI.createURI(resource.getURI().path()
				.replace(".jucm", "_adapted.jucm")));
		// Get the first model element and cast it to the right type, in my
		// example everything is hierarchical included in this first node
		resourceNew.getContents().addAll(resource.getContents());

		// Now save the content.
		try {
			resourceNew.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	private static MessageConsole findConsole() {
		final String consoleName = "AREF Reasoning";
		ConsolePlugin plugin = ConsolePlugin.getDefault();
		IConsoleManager conMan = plugin.getConsoleManager();
		IConsole[] existing = conMan.getConsoles();
		for (int i = 0; i < existing.length; i++)
			if (consoleName.equals(existing[i].getName()))
				return (MessageConsole) existing[i];
		// no console found, so create a new one
		MessageConsole myConsole = new MessageConsole(consoleName, null);
		conMan.addConsoles(new IConsole[] { myConsole });
		return myConsole;
	}

	public static void consoleOut(final String message) {
		if (instance == null) {
			instance = findConsole();
			IWorkbench wb = PlatformUI.getWorkbench();
			IWorkbenchWindow win = wb.getActiveWorkbenchWindow();
			IWorkbenchPage page = win.getActivePage();
			String id = IConsoleConstants.ID_CONSOLE_VIEW;
			IConsoleView view;
			try {
				view = (IConsoleView) page.showView(id);
				view.display(instance);
			} catch (PartInitException e) {

			}
		}
		MessageConsoleStream out = instance.newMessageStream();
		out.println(message);

	}
}
