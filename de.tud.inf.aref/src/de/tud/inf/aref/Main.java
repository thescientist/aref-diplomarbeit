package de.tud.inf.aref;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		OntModel ontology = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF, null);
		ontology.read(args[0], "RDF/XML-ABBREV");

		String queryString = "PREFIX smart-1: <http://wwwpub.zih.tu-dresden.de/~s0254399/smartHome#> "
				+ "SELECT ?curtain ?window WHERE { smart-1:SleepingRoom smart-1:hasDevice ?window . "
				+ "?window <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> smart-1:Window . "
				+ "?window smart-1:hasCurtain ?curtain . ?curtain smart-1:closed ?status . "
				+ "FILTER(?status = false)}";

		Query query = QueryFactory.create(queryString);

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, ontology);
		ResultSet results = qe.execSelect();

		// Output query results
		ResultSetFormatter.out(System.out, results, query);

		// Important - free up resources used running the query
		qe.close();
	}
}
