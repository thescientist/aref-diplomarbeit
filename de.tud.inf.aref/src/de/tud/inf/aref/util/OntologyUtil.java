/*
 * Copyright � 2013 Stefan Prasse. All Rights Reserved. See LICENSE file of
 * project for more information.
 */
package de.tud.inf.aref.util;

import grl.ContextCondition;
import grl.ContextProvider;
import grl.Contribution;
import grl.ElementLink;
import grl.IntentionalElement;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import urncore.GRLmodelElement;
import urncore.Metadata;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;

import de.tud.inf.aref.popup.actions.ReasonerMenuAction;

/**
 * Utility functions for Ontologies using Jena (e.g. loading and querying Urls).
 * 
 * @author Stefan Prasse
 */
public class OntologyUtil {
	private static Map<ContextProvider, OntModel> providerOntologyMapping;

	static {
		providerOntologyMapping = new HashMap<ContextProvider, OntModel>();
	}

	public static void addContextProvider(ContextProvider contextProvider, String ontologySource) {
		if (providerOntologyMapping.containsKey(contextProvider)) {
			return;
		} else {
			OntModel ontology = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF, null);
			ontology.read(ontologySource, "RDF/XML-ABBREV");
			providerOntologyMapping.put(contextProvider, ontology);
		}
	}

	/**
	 * Execute the SPARQL Query of a ContextCondition with Jena. Target of the
	 * query will be the Ontology to the referenced ContextProvider.
	 * 
	 * @param condition
	 *            ContextCondition to reason about.
	 * @return Result of the query.
	 */
	public static QueryResult executeQuery(ContextCondition condition) {

		// TODO bei mehreren Providern?
		ContextProvider provider = (ContextProvider) condition.getProvider().get(0);
		OntModel ontology = providerOntologyMapping.get(provider);
		List<ContextProvider> providerList = condition.getProvider();

		StringBuilder prefix = new StringBuilder();
		for (ContextProvider cProvider : providerList) {
			prefix.append("Prefix ");
			prefix.append(cProvider.getName());
			prefix.append(": <");
			prefix.append(cProvider.getNamespace());
			prefix.append("> ");
		}

		String resolvedQuery = resolveQueryProperties(condition);

		Query query = QueryFactory.create(prefix + resolvedQuery);

		// Execute the query and obtain results
		QueryExecution qe = QueryExecutionFactory.create(query, ontology);
		try {
			ResultSet results = qe.execSelect();
			// ResultSetFormatter.out(System.out, results, query);
			List<String> varNames = results.getResultVars();
			// Output query results
			List<QuerySolution> resultList = ResultSetFormatter.toList(results);
			for (QuerySolution solution : resultList) {
				ReasonerMenuAction.consoleOut(solution.toString());
			}

			if (condition.getElement() instanceof Contribution) {
				if (resultList.size() > 1) {
					return new QueryResult(QueryResultType.CONTRIBUTION, resultList.size());
				} else if (resultList.size() == 1 && varNames.contains("contribution")) {
					return new QueryResult(QueryResultType.CONTRIBUTION, resultList.get(0).get("contribution"));
				} else {
					return new QueryResult(QueryResultType.CONTRIBUTION, 0);
				}
			} else {
				if (resultList.size() == 0) {
					return new QueryResult(QueryResultType.INVALID, null);
				}
				List<String> parameters = GrlUtil.getParametersOfNextElement(condition.getElement());
				if (parameters.isEmpty()) {
					return new QueryResult(QueryResultType.VALID, null);
				} else {
					if (resultList.size() == 1) {
						if (parameters.size() == 1) {
							QuerySolution solution = resultList.get(0);
							return new QueryResult(QueryResultType.ENTITIES, parameters.get(0), solution.get(parameters
									.get(0)));
						}
						Map<String, RDFNode> entitiesMap = new HashMap<String, RDFNode>();
						for (String parameter : parameters) {
							if (!varNames.contains(parameter)) {
								return new QueryResult(QueryResultType.INVALID, null);
							}
							QuerySolution solution = resultList.get(0);
							entitiesMap.put(parameter, solution.get(parameter));
						}
						return new QueryResult(QueryResultType.ENTITIES, entitiesMap);
					} else {
						Map<String, List<RDFNode>> entitiesMap = new HashMap<String, List<RDFNode>>();
						for (String parameter : parameters) {
							if (!varNames.contains(parameter)) {
								return new QueryResult(QueryResultType.INVALID, null);
							}
							if (parameters.size() == 1) {
								List<RDFNode> entities = new LinkedList<RDFNode>();
								for (QuerySolution solution : resultList) {
									entities.add(solution.get(parameter));
								}
								return new QueryResult(QueryResultType.ENTITIES, parameter, entities);
							} else {
								List<RDFNode> entities = new LinkedList<RDFNode>();
								for (QuerySolution solution : resultList) {
									entities.add(solution.get(parameter));
								}
								entitiesMap.put(parameter, entities);
							}
						}
						return new QueryResult(QueryResultType.ENTITIES, entitiesMap);
					}
				}
			}
		} finally {
			// Important - free up resources used running the query
			qe.close();
		}
	}

	/**
	 * @param sparqlExpression
	 * @return
	 */
	private static String resolveQueryProperties(ContextCondition condition) {
		String query = condition.getSparqlExpression();
		List<String> paras = GrlUtil.extractParameters(query);
		if (paras.isEmpty()) {
			return query;
		}
		GRLmodelElement element = condition.getElement();
		if (!(element instanceof ElementLink)) {
			System.out.print("You cant parametrize a context condition of a root element!");
			return query;
		}
		ElementLink link = (ElementLink) element;
		IntentionalElement prevElement;
		if (link instanceof Contribution) {
			prevElement = (IntentionalElement) link.getSrc();
		} else {
			prevElement = (IntentionalElement) link.getDest();
		}
		for (String parameter : paras) {
			String metaDataValues = "";
			for (Object metadata : prevElement.getMetadata()) {
				if (metadata instanceof Metadata) {
					if (((Metadata) metadata).getName().equals(GrlUtil.MetadataNamePrefix + parameter)) {
						metaDataValues = ((Metadata) metadata).getValue();
					}
				}
			}
			if (!metaDataValues.isEmpty()) {
				String[] splittedNames = metaDataValues.split("\\|" + System.getProperty("line.separator"));
				StringBuilder replacement = new StringBuilder();
				for (String instance : splittedNames) {
					replacement.append("?").append(parameter).append("=");
					replacement.append("<").append(instance).append(">");
					replacement.append("||");
				}
				query = query.replace("[" + parameter + "]", replacement.substring(0, replacement.length() - 2));
			}
		}

		return query;
	}
}
