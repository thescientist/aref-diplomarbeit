/*
 * Copyright � 2013 Stefan Prasse. All Rights Reserved. See LICENSE file of
 * project for more information.
 */
package de.tud.inf.aref.util;

/**
 * @author Stefan Prasse
 * 
 */
public enum QueryResultType {
	VALID, INVALID, ENTITIES, CONTRIBUTION
}
