/*
 * Copyright � 2013 Stefan Prasse. All Rights Reserved. See LICENSE file of
 * project for more information.
 */
package de.tud.inf.aref.util;

/**
 * @author Stefan Prasse
 * 
 */
public class QueryResult {

	private QueryResultType type;
	private Object value;
	private String parameterName;

	public QueryResult(QueryResultType type, Object value) {
		this.type = type;
		this.value = value;
	}

	public QueryResult(QueryResultType type, String paramertName, Object value) {
		this.type = type;
		this.value = value;
		this.parameterName = paramertName;
	}

	public QueryResultType getType() {
		return type;
	}

	public Object getValue() {
		return value;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
}
