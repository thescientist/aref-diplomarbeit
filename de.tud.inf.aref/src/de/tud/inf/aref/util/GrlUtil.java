/*
 * Copyright � 2013 Stefan Prasse. All Rights Reserved. See LICENSE file of
 * project for more information.
 */
package de.tud.inf.aref.util;

import grl.BeliefLink;
import grl.Contribution;
import grl.Decomposition;
import grl.Dependency;
import grl.ElementLink;
import grl.GRLLinkableElement;
import grl.GRLspec;
import grl.IntentionalElement;
import grl.IntentionalElementType;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.preference.IPreferenceStore;

import seg.jUCMNav.model.commands.delete.DeleteElementLinkCommand;
import seg.jUCMNav.model.commands.delete.DeleteIntentionalElementCommand;
import seg.jUCMNav.model.util.MetadataHelper;
import seg.jUCMNav.views.preferences.DeletePreferences;
import urn.URNspec;
import urncore.GRLmodelElement;

import com.hp.hpl.jena.rdf.model.RDFNode;

import de.tud.inf.aref.popup.actions.ReasonerMenuAction;

/**
 * Class holding utility functions for GRLspec manipulation.
 * 
 * @author Stefan Prasse
 * 
 */
public class GrlUtil {

	public static final String MetadataNamePrefix = "AREF:";

	// TODO
	// private static Map<IntentionalElement, Map<String, List<RDFNode>>>
	// parametrization = new HashMap<IntentionalElement, Map<String,
	// List<RDFNode>>>();

	/**
	 * Get all Root Intentional Elements of a GRL Specification. Root elements
	 * are those that are not Softgoals and don't have outgoing Decomposition
	 * Links.
	 * 
	 * @param model
	 *            GRL Specification
	 * @return List of root elements.
	 */
	public static List<IntentionalElement> getRootElements(GRLspec model) {
		List<IntentionalElement> out = new LinkedList<IntentionalElement>();
		for (Object element : model.getIntElements()) {
			if (element instanceof IntentionalElement) {
				IntentionalElement intElement = (IntentionalElement) element;
				if (intElement.getType() == IntentionalElementType.SOFTGOAL_LITERAL || isDependum(intElement))
					continue;
				@SuppressWarnings("unchecked")
				EList<ElementLink> sources = intElement.getLinksSrc();
				if (sources == null || sources.isEmpty()) {
					out.add(intElement);
				} else {
					boolean onlyContribution = true;
					for (ElementLink source : sources) {
						if (!(source instanceof Contribution) && !(source instanceof BeliefLink)
								&& !(source instanceof Dependency)) {
							onlyContribution = false;
							break;
						}
					}
					if (onlyContribution)
						out.add(intElement);
				}
			}
		}
		return out;
	}

	/**
	 * Reason about an intentional element and its links. Called recursive for
	 * linked elements.
	 * 
	 * @param spec
	 *            URN Specification, needed to adapt graphical representation.
	 * @param intentionalElement
	 *            Intentional Element to reason about.
	 * @param isRoot
	 *            Indicates if this element is a root element.
	 */
	public static void reason(URNspec spec, IntentionalElement intentionalElement, boolean isRoot) {
		ReasonerMenuAction.consoleOut("Reason about: " + intentionalElement.getName());
		if (isRoot) {
			// TODO Reasoning about annotated root Element
		}
		// Reason about conditions of outgoing Links and follow them
		List<ElementLink> outgoingLinks = new LinkedList<ElementLink>();
		outgoingLinks.addAll(intentionalElement.getLinksSrc());
		for (ElementLink elementLink : outgoingLinks) {
			if (elementLink.getCondition() != null) {
				ReasonerMenuAction.consoleOut("Condition found at: " + elementLink.getName());
				QueryResult reasonResult = OntologyUtil.executeQuery(elementLink.getCondition());
				handleResult(elementLink, reasonResult);
			}
			if (!(elementLink instanceof Decomposition) && !(elementLink instanceof Dependency)) {
				// follow contributions
				GRLLinkableElement destination = elementLink.getDest();
				if (destination instanceof IntentionalElement) {
					reason(spec, (IntentionalElement) destination, false);
				}
			}
		}
		// If incoming Links are Decomposition or Dependency call this function
		// recursive.
		@SuppressWarnings("unchecked")
		List<ElementLink> incomingLinks = new LinkedList<ElementLink>();
		incomingLinks.addAll(intentionalElement.getLinksDest());
		for (ElementLink elementLink : incomingLinks) {
			if (elementLink instanceof Decomposition || elementLink instanceof Dependency
					&& !isDependum(intentionalElement)) {
				GRLLinkableElement source = elementLink.getSrc();
				if (source instanceof IntentionalElement) {
					reason(spec, (IntentionalElement) source, false);
				}
			}
		}
	}

	/**
	 * Handles the result, depending on the ElementLink type.
	 * 
	 * @param elementLink
	 *            ElementLink holding the ContextCondition.
	 * @param reasonResult
	 *            Result of the ContextCondition.
	 */
	@SuppressWarnings("unchecked")
	private static void handleResult(ElementLink elementLink, QueryResult reasonResult) {
		URNspec urn = elementLink.getGrlspec().getUrnspec();
		switch (reasonResult.getType()) {
		case CONTRIBUTION: {
			Contribution contribution = (Contribution) elementLink;
			contribution.setQuantitativeContribution((Integer) reasonResult.getValue());
			ReasonerMenuAction.consoleOut("Contribution set by Condition.");
			break;
		}
		case ENTITIES: {
			// TODO save in Map
			ReasonerMenuAction.consoleOut("Condition returned entities.");
			IntentionalElement next;
			if (elementLink instanceof Decomposition || elementLink instanceof Dependency) {
				next = (IntentionalElement) elementLink.getSrc();
			} else {
				next = (IntentionalElement) elementLink.getDest();
			}
			if (reasonResult.getValue() instanceof RDFNode) {
				MetadataHelper.addMetaData(urn, next, MetadataNamePrefix + reasonResult.getParameterName(),
						reasonResult.getValue().toString());
			} else if (reasonResult.getValue() instanceof Map) {
				// TODO
			} else if (reasonResult.getValue() instanceof List) {
				List<RDFNode> entities = (List<RDFNode>) reasonResult.getValue();
				StringBuilder value = new StringBuilder();
				for (RDFNode node : entities) {
					value.append(node.toString());
					value.append("|").append(System.getProperty("line.separator"));
				}
				MetadataHelper.addMetaData(urn, next, MetadataNamePrefix + reasonResult.getParameterName(),
						value.toString());
			}
			break;
		}
		case INVALID: {
			ReasonerMenuAction.consoleOut("Condition invalid.");
			// Nachfrage deaktivieren
			IPreferenceStore preferences = DeletePreferences.getPreferenceStore();
			String currentPrefValue = preferences.getString(DeletePreferences.PREF_DELREFERENCE);
			preferences.setValue(DeletePreferences.PREF_DELREFERENCE, DeletePreferences.PREF_ALWAYS);
			// Modell-Elemente l�schen
			IntentionalElement next = null;
			if (elementLink instanceof Contribution) {
				elementLink.getSrc().getLinksSrc().remove(elementLink);
				if (elementLink.getDest() instanceof IntentionalElement) {
					next = (IntentionalElement) elementLink.getDest();
					next.getLinksDest().remove(elementLink);
				}
			} else if (elementLink instanceof Decomposition || elementLink instanceof Dependency) {
				elementLink.getDest().getLinksDest().remove(elementLink);
				if (elementLink.getSrc() instanceof IntentionalElement) {
					next = (IntentionalElement) elementLink.getSrc();
					next.getLinksSrc().remove(elementLink);
				}
			}
			DeleteElementLinkCommand deleteCmd = new DeleteElementLinkCommand(elementLink);
			if (deleteCmd.canExecute()) {
				deleteCmd.execute();
			}
			if (next != null && !hasIncomingLinks(next))
				removeModelElements(next, true);
			// set back deletion preferences
			preferences.setValue(DeletePreferences.PREF_DELREFERENCE, currentPrefValue);
			break;
		}
		case VALID: {
			// Nothing to do here.
			ReasonerMenuAction.consoleOut("Condition valid.");
			break;
		}
		}

	}

	/**
	 * Removes a model element from grlSpec.
	 * 
	 * @param element
	 *            GrlModelElement to remove.
	 * @param recursive
	 *            Should following elements also be removed.
	 */
	private static void removeModelElements(IntentionalElement element, boolean recursive) {
		List<IntentionalElement> toRemove = new LinkedList<IntentionalElement>();
		if (recursive) {
			for (Object link : element.getLinksSrc()) {
				if (link instanceof Contribution) {
					Contribution elementLink = (Contribution) link;
					Object dest = elementLink.getDest();
					if (dest instanceof IntentionalElement) {
						toRemove.add((IntentionalElement) dest);
					}
				}
			}
			for (Object link : element.getLinksDest()) {
				if (link instanceof Dependency || link instanceof Decomposition) {
					ElementLink elementLink = (ElementLink) link;
					Object src = elementLink.getSrc();
					if (src instanceof IntentionalElement) {
						toRemove.add((IntentionalElement) src);
					}
				}
			}
		}

		DeleteIntentionalElementCommand deleteCmd = new DeleteIntentionalElementCommand(element);
		if (deleteCmd.canExecute()) {
			deleteCmd.execute();
		}
		for (IntentionalElement next : toRemove) {
			if (!hasIncomingLinks(next))
				removeModelElements(next, recursive);
		}
	}

	/**
	 * Check if an IntentionalElement has incoming links.
	 * 
	 * @param intElement
	 *            IntentionalElement to check.
	 * @return true if incoming links exist.
	 */
	private static boolean hasIncomingLinks(IntentionalElement intElement) {
		for (Object object : intElement.getLinksDest()) {
			if (object instanceof Contribution || object instanceof BeliefLink) {
				return true;
			}
		}
		for (Object object : intElement.getLinksSrc()) {
			if (object instanceof Decomposition || object instanceof Dependency) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Test if an intentional Element is a Dependum, by testing its incoming and
	 * outgoing links.
	 * 
	 * @param intentionalElement
	 *            Element to check.
	 * @return true if this element is a Dependum, false otherwise.
	 */
	public static boolean isDependum(IntentionalElement intentionalElement) {
		@SuppressWarnings("unchecked")
		EList<ElementLink> outgoingLinks = intentionalElement.getLinksSrc();
		if (outgoingLinks.size() == 1 && outgoingLinks.get(0) instanceof Dependency) {
			@SuppressWarnings("unchecked")
			EList<ElementLink> incomingLinks = intentionalElement.getLinksDest();
			if (incomingLinks.size() == 1 && incomingLinks.get(0) instanceof Dependency) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Get a List of parameters of a model element (if it's an intentional
	 * element) or the next intentional element (if this is an element link)
	 * with a condition.
	 * 
	 * @param element
	 *            ElementLink or IntentionalElement
	 * @return List of parameters (may be empty if element not parameterized).
	 */
	public static List<String> getParametersOfNextElement(GRLmodelElement element) {
		String toParse = "";
		// root element
		if (element instanceof IntentionalElement) {
			IntentionalElement intElement = (IntentionalElement) element;
			toParse = intElement.getName();
		} else if (element instanceof ElementLink) {
			// annotated Link. Check next Element
			ElementLink link = (ElementLink) element;
			IntentionalElement next = null;
			if (link instanceof Dependency || link instanceof Decomposition) {
				if (link.getSrc() instanceof IntentionalElement) {
					next = (IntentionalElement) link.getSrc();
				}
			} else {
				if (link.getDest() instanceof IntentionalElement) {
					next = (IntentionalElement) link.getDest();
				}
			}
			if (next != null) {
				toParse = next.getName();
			}
		}
		return extractParameters(toParse);
	}

	/**
	 * Parse a string for any words in squared brackets via regular expression
	 * "[\w]" and return all found Strings as a list.
	 * 
	 * @param toParse
	 *            String to parse for parameter declarations.
	 * @return List of parameter names.
	 */
	public static List<String> extractParameters(String toParse) {
		List<String> parameters = new LinkedList<String>();
		if (toParse == null || toParse.isEmpty()) {
			return parameters;
		}
		// Any word between squared brackets is seen as parameter declaration.
		Pattern pattern = Pattern.compile(Pattern.quote("[") + "\\w+" + Pattern.quote("]"));
		Matcher m = pattern.matcher(toParse);
		while (m.find()) {
			String para = m.group();
			parameters.add(para.substring(1, para.length() - 1));
		}
		return parameters;
	}
}
